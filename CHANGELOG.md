# Changelog

Todos los cambios notables del proyecto se documentan en este archivo.

El formato se basa en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [] - 2020-11-23

### Added

- Endpoint creación usuarios
- Endpoint lista usuarios
- Endpoint eliminar usuario
- Endpoint geocodificación dirección usuarios
- Agrupamiento resultados disponibles en Excel
