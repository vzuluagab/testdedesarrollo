from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UsuariosSerializer
from .models import Usuarios
import requests


class UsuariosCreateApi(generics.CreateAPIView):
    """Creacion un usuario almacenando nombre, apellido, dirección y ciudad"""
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer


class UsuariosListApi(generics.ListAPIView):
    """Lista los usuarios disponibles en la base de datos con sus diferentes atributos"""
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer


class UsuariosSingleApi(generics.RetrieveAPIView):
    """Retorna el usuario con sus atributos que coincida con el id enviado en la URL"""
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer


class UsuariosDeleteApi(generics.DestroyAPIView):
    """Elimina el usuario que coincida con el id recibido en la URL"""
    queryset = Usuarios.objects.all()
    serializer_class = UsuariosSerializer


class UsuariosGeocodingApi(APIView):
    """Actualiza los atributos latitud y longitud de los usuarios que no tienen
    valores asignados en estos atributos y retorna el listado de usuarios actualizados"""
    API_KEY = 'AIzaSyCSycceFPmm0_tGZB9Xru7RzbY9ZtDggv0'

    def geocodeUsuarioAddress(self, usuario):
        """Obtiene latitud y longitud de una direccion con el servicio geocode de Google
        Parameters
        ----------
        usuario : Usuarios
            Modelo usuario al cual se actualiza latitud y longitud
        """
        stringUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address={ciudad}+{direccion}&key={key}'
        stringCiudad = usuario.ciudad.replace(" ", "+")
        stringDireccion = usuario.direccion.replace(" ", "+")
        geocodeUrl = stringUrl.format(
            ciudad=stringCiudad, direccion=stringDireccion, key=self.API_KEY)
        results = requests.get(geocodeUrl)
        results = results.json()
        if len(results['results']) == 0:
            usuario.latitud = 0
            usuario.longitud = 0
        else:
            usuario.latitud = results['results'][0]['geometry']['location']['lat']
            usuario.longitud = results['results'][0]['geometry']['location']['lng']
            usuario.estadogeo = True
        usuario.save()

    def get(self, request):
        usuarios = Usuarios.objects.filter(latitud=None, longitud=None)
        for usuario in usuarios:
            self.geocodeUsuarioAddress(usuario)
        data = UsuariosSerializer(usuarios, many=True).data
        return Response(data)
