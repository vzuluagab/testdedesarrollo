from django.db import models


class Usuarios(models.Model):
    """Modelo Usuarios"""
    nombre = models.CharField(max_length=80)
    apellido = models.CharField(max_length=80)
    direccion = models.CharField(max_length=120)
    ciudad = models.CharField(max_length=80)
    longitud = models.FloatField(null=True)
    latitud = models.FloatField(null=True)
    estadogeo = models.BooleanField(default=False, null=True)

    def __str__(self):
        return self.nombre
