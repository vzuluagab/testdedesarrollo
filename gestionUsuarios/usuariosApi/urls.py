from django.urls import path
from .views import UsuariosCreateApi, UsuariosListApi, UsuariosSingleApi, UsuariosDeleteApi, UsuariosGeocodingApi

urlpatterns = [
    path('api/crear',UsuariosCreateApi.as_view()),
    path('api/lista',UsuariosListApi.as_view()),
    path('api/usuario/<int:pk>',UsuariosSingleApi.as_view()),
    path('api/eliminar/<int:pk>',UsuariosDeleteApi.as_view()),
    path('api/geocodificar_base',UsuariosGeocodingApi.as_view()),
]
