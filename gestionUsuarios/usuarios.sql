CREATE TABLE "usuariosApi_usuarios" (
  "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  "nombre" varchar(80) NOT NULL,
  "apellido" varchar(80) NOT NULL,
  "direccion" varchar(120) NOT NULL,
  "ciudad" varchar(80) NOT NULL,
  "longitud" real NULL,
  "latitud" real NULL,
  "estadogeo" bool NULL)
