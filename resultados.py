import pandas
import xlrd
import openpyxl


class Resultados:
    """Clase responsable del analisis de los resultados de votaciones"""

    votacion = pandas.read_excel('resultadosVotacion.xlsx')

    def agruparPor(self, parametroAgrupacion):
        """Agrupa los resultados de la votacion por el parametro indicado
        Parameters
        ----------
        parametroAgrupacion : string
            Indica la columna por la cual se va a agrupar y totalizar el conjunto de datos
        """
        columnasResultado = self.votacion.columns.tolist()
        if parametroAgrupacion in columnasResultado:
            agrupacion = self.votacion.groupby(parametroAgrupacion, sort=False)[
                'votos'].sum().reset_index()
            agrupacion.to_excel(parametroAgrupacion + '.xlsx', index=False)


resultadoElecciones = Resultados()
resultadoElecciones.agruparPor('candidato')
resultadoElecciones.agruparPor('partido')
resultadoElecciones.agruparPor('puesto')
resultadoElecciones.agruparPor('mpio')
resultadoElecciones.agruparPor('depto')
