# TestDeDesarrollo - Gestión Usuarios

Solución propuesta al test de desarrollo en la cual se incluye API REST que permite crear,
listar y eliminar usuarios de la base de datos. Además, análisis del contenido de archivo
de Excel


## Comenzando

Estas intrucciones permiten obtener una copia del proyecto

Clonar repositorio: `git clone https://gitlab.com/vzuluagab/testdedesarrollo.git`


### Prerequisitos

* Python >= 3.5 `https://www.python.org/downloads/`
* Django versión 3.1.3 `https://www.djangoproject.com/download/`
* Django REST Framework `https://www.django-rest-framework.org/#installation`
* Pandas versión 1.1.4 `https://pandas.pydata.org/`
* requests `https://requests.readthedocs.io/en/master/`
* openpyxl versión 3.0.5 `https://openpyxl.readthedocs.io/en/stable/`
* xlrd `https://xlrd.readthedocs.io/en/latest/`


### Instalación

* Clonar repositorio
* El gestor de base de datos seleccionado para el proyecto es SQLite, el cual es el gestor usado en Django
por defecto. La base de datos se encuentra disponible en `gestionUsuarios/gestionUsuarios/db.sqlite3`
* En el directorio `gestionUsuarios/gestionUsuarios` ejecutar el comando `python manage.py migrate` para
realizar migraciones
* En el directorio `gestionUsuarios/gestionUsuarios` ejecutar el comando `python manage.py runserver` para
iniciar el servidor


### Endpoints

* Crear usuario: `POST http://127.0.0.1:8000/usuario/api/crear`
* Listar usuarios `GET http://127.0.0.1:8000/usuario/api/lista`
* Obtener usuario: `GET http://127.0.0.1:8000/usuario/api/usuario/3`
* Eliminar Usuario: `DELETE http://127.0.0.1:8000/usuario/api/eliminar/3`
* Geocodificar dirección de usuario: `GET http://127.0.0.1:8000/usuario/api/geocodificar_base`


# Análisis Excel

Para realizar el análisis de los resultados de las votaciones disponibles en gestionUsuarios/resultadosVotacion.xlsx ejecutar el comando `python resultados.py`
en el directorio gestionUsuarios


## Autor

* Ver lista de [contribuyentes](https://gitlab.com/vzuluagab/testdedesarrollo/-/project_members).
